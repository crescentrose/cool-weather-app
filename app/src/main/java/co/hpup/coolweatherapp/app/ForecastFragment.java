package co.hpup.coolweatherapp.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ForecastFragment extends Fragment {

    protected ArrayAdapter<String> mForecastAdapter;

    public ForecastFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mForecastAdapter = new ArrayAdapter<>(
                getActivity(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textview,
                new ArrayList<String>());

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        ListView forecastView = (ListView) rootView.findViewById(R.id.listview_forecast);
        forecastView.setAdapter(mForecastAdapter);
        forecastView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent detailIntent = new Intent(getActivity(), DetailActivity.class)
                        .putExtra(Intent.EXTRA_TEXT, mForecastAdapter.getItem(i));
                startActivity(detailIntent);

            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            return updateWeather();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateWeather();
    }

    private boolean updateWeather() {
        // get zipcode from settings
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String zip = preferences.getString("location", "10000,HR");
        String convertToImperial = preferences.getString("unit", "false");
        FetchWeatherTask task = new FetchWeatherTask();
        task.execute(zip, convertToImperial);
        return true;
    }

    public class FetchWeatherTask extends AsyncTask<String, Void, String[]> {
        private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();

        private double metricToImperial(double metric) {
            return metric * 1.8 + 32;
        }

        @Override
        protected void onPostExecute(String[] strings) {
            if (strings != null) {
                mForecastAdapter.clear();
                List<String> forecast = new ArrayList<>(Arrays.asList(strings));
                mForecastAdapter.addAll(forecast);
            }
        }

        @Override
        protected String[] doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            final int daysToFetch = 7;
            String forecastJson;

            try {
                Uri builtUri = Uri.parse("http://api.openweathermap.org/data/2.5/forecast/daily").buildUpon()
                        .appendQueryParameter("zip", params[0])
                    .appendQueryParameter("APPID", BuildConfig.WEATHER_API_TOKEN)
                    .appendQueryParameter("units", "metric")
                    .appendQueryParameter("mode", "json")
                    .appendQueryParameter("cnt", Integer.toString(daysToFetch))
                    .build();

                URL url = new URL(builtUri.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream stream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (stream == null) {
                    return null;
                }

                reader = new BufferedReader(new InputStreamReader(stream));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                if (buffer.length() == 0) {
                    return null;
                }
                forecastJson = buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream ", e);
                    }
                }
            }
            try {
                return jsonToData(forecastJson, daysToFetch, Boolean.parseBoolean(params[1]));
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Can't parse JSON!", e);
                return null;
            }
        }

        private String getReadableDateString(long time) {
            SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("EEE MMM dd");
            return shortenedDateFormat.format(time);
        }
        private String formatHighLows(double high, double low) {
            long roundedHigh = Math.round(high);
            long roundedLow = Math.round(low);

            return roundedHigh + "/" + roundedLow;
        }

        private String[] jsonToData(String jsonForecast, int days, boolean useImperial) throws JSONException {
            // get the list from the json output
            JSONArray weatherConditions = (new JSONObject(jsonForecast)).getJSONArray("list");

            // ???
            Log.v(LOG_TAG, "Got JSON " + jsonForecast);

            // some udacity weirdness
            Time dayTime = new Time();
            dayTime.setToNow();
            int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);
            dayTime = new Time();

            String[] result = new String[days];
            for (int i = 0; i < weatherConditions.length(); i++) {
                String day, description, temps;
                JSONObject jsonTemps;
                long dateTime;
                dateTime = dayTime.setJulianDay(julianStartDay + i);
                day = getReadableDateString(dateTime);

                description = weatherConditions.getJSONObject(i).getJSONArray("weather").getJSONObject(0).getString("description");
                jsonTemps = weatherConditions.getJSONObject(i).getJSONObject("temp");

                double tempMin = jsonTemps.getDouble("min");
                double tempMax = jsonTemps.getDouble("max");
                if (useImperial) {
                    temps = formatHighLows(metricToImperial(tempMin), metricToImperial(tempMax));
                } else {
                    temps = formatHighLows(tempMin, tempMax);
                }

                result[i] = day + " - " + description + " - " + temps;
            }

            return result;
        }
    }
}
